terraform {
  source = "git@code.europa.eu:simpl/Operations/terraform-modules.git//network?ref=network-v0.0.1"
}

locals {
  # Load the relevant env.hcl file based on where terragrunt was invoked. This works because find_in_parent_folders
  # always works at the context of the child configuration.
  env_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  env_name = local.env_vars.locals.env

  instance_vars = read_terragrunt_config(find_in_parent_folders("instance.hcl"))
  instance_name = local.instance_vars.locals.instance

  region_vars = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  region_name = local.region_vars.locals.region
  region_global_name = local.region_vars.locals.region_global

  project_vars = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  project_id = local.project_vars.locals.project-id 

  global_prefix = "${local.env_name}-${local.region_name}-${local.instance_name}"
}

inputs = {
  # network
  network-private-name = "${local.global_prefix}-private-network"
  network-private-regions = [upper(local.region_name)]
  network-private-region = upper(local.region_name)
  service-name = local.project_id
}

