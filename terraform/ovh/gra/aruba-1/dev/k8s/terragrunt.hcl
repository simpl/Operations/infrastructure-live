include "root" {
  path = find_in_parent_folders()
  expose = true
}

include "env" {
  path = "${get_terragrunt_dir()}/../../../../../_env/k8s.hcl"
}

inputs = {
  # K8S
  k8s-project-name = "DEV-Aruba"
  k8s-project-default-vrack-gateway = ""
  k8s-project-private_network_routing_as_default = false
  k8s-project-version = "1.29"
  k8s-project-nodepool-flavor-name = "b2-7"
  k8s-project-nodepool-desired-nodes = 0
  k8s-project-nodepool-max-nodes = 0
  k8s-project-nodepool-min-nodes = 0
  k8s-project-nodepool-autoscale = false
}

