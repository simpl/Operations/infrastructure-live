include "root" {
  path = find_in_parent_folders()
  expose = true
}

include "env" {
  path = "${get_terragrunt_dir()}/../../../../../_env/network.hcl"
}

inputs = {
  # network
  network-private-vlan-id = 168
  network-private-subnet-start = "10.4.0.2"
  network-private-subnet-end = "10.4.255.254"
  network-private-subnet = "10.4.0.0/16"
  network-private-subnet-dhcp = true
  network-private-subnet-no-gateway = false
}

