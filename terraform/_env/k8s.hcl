terraform {
  source = "git@code.europa.eu:simpl/Operations/terraform-modules.git//k8s?ref=k8s-v0.0.6"
}

locals {
  # Load the relevant env.hcl file based on where terragrunt was invoked. This works because find_in_parent_folders
  # always works at the context of the child configuration.
  env_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  env_name = local.env_vars.locals.env

  instance_vars = read_terragrunt_config(find_in_parent_folders("instance.hcl"))
  instance_name = local.instance_vars.locals.instance

  region_vars = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  region_name = local.region_vars.locals.region
  region_global_name = local.region_vars.locals.region_global

  project_vars = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  project_id = local.project_vars.locals.project-id 

  global_prefix = "${local.env_name}-${local.region_name}-${local.instance_name}"
}

dependency "network" {
  config_path = "../network"

  # Configure mock outputs for the `validate` command that are returned when there are no outputs available (e.g the
  # module hasn't been applied yet.
  mock_outputs_allowed_terraform_commands = ["init","plan","validate"]
  mock_outputs = {
    network-private-openstackid-out = "dummy-network-openstackid"
  }
  mock_outputs_merge_strategy_with_state = "shallow"
}

dependencies {
  paths = ["../network","../gateway"]
}

inputs = {
  service-name = local.project_id
  k8s-project-name = "${local.env_name}-${local.instance_name}"
  k8s-project-region = upper(local.region_name)
  k8s-project-network-openstackid = dependency.network.outputs.network-private-openstackid-out
  k8s-project-nodepool-name = "${local.env_name}-${local.instance_name}-nodepool"
}

