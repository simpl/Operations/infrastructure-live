include "root" {
  path = find_in_parent_folders()
  expose = true
}

include "env" {
  path = "${get_terragrunt_dir()}/../../../../../_env/kubectl.hcl"
}

inputs = {
  kubectl-manifest-config-path = "config/*.yaml" 
}
