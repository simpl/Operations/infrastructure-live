include "root" {
  path = find_in_parent_folders()
  expose = true
}

include "env" {
  path = "${get_terragrunt_dir()}/../../../../../_env/network.hcl"
}

inputs = {
  # network
  network-private-vlan-id = 1114
  network-private-subnet-start = "10.6.0.2"
  network-private-subnet-end = "10.6.0.254"
  network-private-subnet = "10.6.0.0/16"
  network-private-subnet-dhcp = true
  network-private-subnet-no-gateway = false
}

