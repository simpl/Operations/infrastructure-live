locals {
  state-name = replace(path_relative_to_include(), "/", "-")
}

remote_state {
  backend = "http"
  generate = {
    path = "state.tf"
    if_exists = "overwrite_terragrunt"
  }

  config = {
    address="${get_env("TF_ADDRESS")}/${local.state-name}"
    lock_address="${get_env("TF_ADDRESS")}/${local.state-name}/lock"
    unlock_address="${get_env("TF_ADDRESS")}/${local.state-name}/lock"
    username=get_env("TF_USERNAME")
    password=get_env("TF_PASSWORD")
    lock_method="POST"
    unlock_method="DELETE"
    retry_wait_min=5
  }
}

generate "provider" {
  path = "provider.tf"
  if_exists = "overwrite_terragrunt"

  contents = <<EOF
provider "ovh" {
  endpoint           = "ovh-eu"
  }
EOF
}

