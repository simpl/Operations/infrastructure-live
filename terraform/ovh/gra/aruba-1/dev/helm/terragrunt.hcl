include "root" {
  path = find_in_parent_folders()
  expose = true
}

include "env" {
  path = "${get_terragrunt_dir()}/../../../../../_env/helm.hcl"
}

inputs = {
  helm-release  = {
    "cert-manager" = {
      helm-release-version = "v1.15.3"
      helm-release-namespace = "cert-manager"
      helm-release-create-namespace = true
      helm-release-name = "cert-manager"
      helm-release-repository = "https://charts.jetstack.io"
      helm-release-chart = "cert-manager"
      helm-release-timeout = 1000
      helm-release-custom-config-files = [
        file("values/cert-manager-values.yaml")
      ]
      custom-sets = {
        "dummy" = {
          set-name = ""
          set-value = ""
        }
      }
    }

  "ingress-nginx" = {
    helm-release-version = "4.5.2"
    helm-release-namespace = "ingress-nginx"
    helm-release-create-namespace = true
    helm-release-name = "ingress-nginx"
    helm-release-repository = "https://kubernetes.github.io/ingress-nginx"
    helm-release-chart = "ingress-nginx"
    helm-release-timeout = 1000
    helm-release-custom-config-files = [
      file("values/ingress-nginx-values.yaml")
    ]
    custom-sets = {
      "dummy" = {
        set-name = ""
        set-value = ""
      }
    }
  }


  "argo-cd" = {
    helm-release-version = "7.1.4"
    helm-release-namespace = "argocd"
    helm-release-create-namespace = true
    helm-release-name = "argo-cd"
    helm-release-repository = "https://argoproj.github.io/argo-helm"
    helm-release-chart = "argo-cd"
    helm-release-timeout = 1000
    helm-release-custom-config-files = [
      file("values/argocd-values.yaml")
    ]
    custom-sets = {
      "dummy" = {
        set-name = ""
        set-value = ""
      }
    }
  }

  }
}
