include "root" {
  path = find_in_parent_folders()
  expose = true
}

include "env" {
  path = "${get_terragrunt_dir()}/../../../../../_env/helm.hcl"
}

dependency "database" {
  config_path = "../database"

  # Configure mock outputs for the `validate` command that are returned when there are no outputs available (e.g the
  # module hasn't been applied yet.
  mock_outputs_allowed_terraform_commands = ["init","plan","validate"]
  mock_outputs = {
    database-postgresql-user-name-output = ["dummy-username"]
    database-postgresql-user-password-output = ["dummy-password"]
    database-database-name-output = ["dummy-name"]
    database-project-endpoints-domain = ["dummy-domain"]
    database-project-endpoints-port = ["dummy-port"]
    database-project-endpoints-scheme = ["dummy-scheme"]
  }
  mock_outputs_merge_strategy_with_state = "shallow"
}

inputs = {
  helm-release  = {
    "cert-manager" = {
      helm-release-version = "v1.15.3"
      helm-release-namespace = "cert-manager"
      helm-release-create-namespace = true
      helm-release-name = "cert-manager"
      helm-release-repository = "https://charts.jetstack.io"
      helm-release-chart = "cert-manager"
      helm-release-custom-config-files = [
        file("values/cert-manager-values.yaml")
      ]
      helm-release-timeout = 1000
      custom-sets = {
        "dummy" = {
          set-name = ""
          set-value = ""
        }
      }
    }

    "ingress-nginx" = {
      helm-release-version = "4.5.2"
      helm-release-namespace = "ingress-nginx"
      helm-release-create-namespace = true
      helm-release-name = "ingress-nginx"
      helm-release-repository = "https://kubernetes.github.io/ingress-nginx"
      helm-release-chart = "ingress-nginx"
      helm-release-custom-config-files = [
        file("values/ingress-nginx-values.yaml")
      ]
      helm-release-timeout = 1000
      custom-sets = {
        "dummy" = {
          set-name = ""
          set-value = ""
        }
      }
    }

    "keycloak" = {
      helm-release-version = "24.4.4"
      helm-release-namespace = "keycloak"
      helm-release-create-namespace = true
      helm-release-name = "keycloak"
      helm-release-repository = "oci://registry-1.docker.io/bitnamicharts"
      helm-release-chart = "keycloak"
      helm-release-timeout = 5000
      helm-release-custom-config-files = [
        templatefile("values/keycloak-values.yaml", {
          db_user = dependency.database.outputs.database-postgresql-user-name-output[0],
          db_password = dependency.database.outputs.database-postgresql-user-password-output[0],
          db_database = dependency.database.outputs.database-database-name-output[0],
          db_port = dependency.database.outputs.database-project-endpoints-port[0],
          db_addr = dependency.database.outputs.database-project-endpoints-domain[0],
          db_vendor = dependency.database.outputs.database-project-endpoints-scheme[0]
          })
      ]
      custom-sets = {
        "dummy" = {
          set-name = ""
          set-value = ""
        }
      }
    }
  }
}
