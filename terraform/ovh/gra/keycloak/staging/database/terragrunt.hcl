include "root" {
  path = find_in_parent_folders()
  expose = true
}

terraform {
  source = "git@code.europa.eu:simpl/Operations/terraform-modules.git//database?ref=database-v0.0.2"
}

locals {
  # Load the relevant env.hcl file based on where terragrunt was invoked. This works because find_in_parent_folders
  # always works at the context of the child configuration.
  env_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  env_name = local.env_vars.locals.env

  instance_vars = read_terragrunt_config(find_in_parent_folders("instance.hcl"))
  instance_name = local.instance_vars.locals.instance

  region_vars = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  region_name = local.region_vars.locals.region
  region_global_name = local.region_vars.locals.region_global

  project_vars = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  project_id = local.project_vars.locals.project-id 

  global_prefix = "${local.env_name}-${local.region_name}-${local.instance_name}"
}

dependency "network" {
  config_path = "../network"

  # Configure mock outputs for the `validate` command that are returned when there are no outputs available (e.g the
  # module hasn't been applied yet.
  mock_outputs_allowed_terraform_commands = ["init","plan","validate"]
  mock_outputs = {
    network-private-openstackid-out = "dummy-database-openstackid"
    network-private-subnet-id-out = "dummy-database-subnetid"
  }
  mock_outputs_merge_strategy_with_state = "shallow"
}

dependencies {
  paths = ["../network","../gateway","../k8s"]
}

inputs = {
  custom-nodes = {
    "postgresql-node-1" = {
      database-project-nodes-region = upper(local.region_global_name)
      database-project-nodes-network-id = dependency.network.outputs.network-private-openstackid-out
      database-project-nodes-subnet-id = dependency.network.outputs.network-private-subnet-id-out
    }
    "postgresql-node-2" = {
      database-project-nodes-region = upper(local.region_global_name)
      database-project-nodes-network-id = dependency.network.outputs.network-private-openstackid-out
      database-project-nodes-subnet-id = dependency.network.outputs.network-private-subnet-id-out
    }
  }
  database-project  = {
    "postgresql" = {
      database-project-service-name = "ea5f1127cb1f407899c1e91d490b34e5"
      database-project-description = "Postgresql database for keycloak"
      database-project-engine = "postgresql"
      database-project-version = "16"
      database-project-plan = "business"
      database-project-flavor = "db1-4"
      custom-ip-restriction = {
        "node-ip-restriction" = {
            database-ip-restriction-description = "ip restriction for pods"
            database-ip-restriction-ip = "10.2.0.0/16"
          }
        "service-ip-restriction" = {
            database-ip-restriction-description = "ip restriction for services"
            database-ip-restriction-ip = "10.3.0.0/16"
        }
        "k8s-ip-restriction" = {
            database-ip-restriction-description = "ip restriction for k8s"
            database-ip-restriction-ip = "10.4.0.0/16"
        }
      }
    }
  }
  database-postgresql-users = {
    "keycloak" = {
      database-postgresql-user-name = "keycloak"
      database-postgresql-user-roles = ["replication"]
      database-key = "postgresql"
    }
  }
  database-database = {
    "keycloak-db" = {
      database-database-name = "keycloak-db"
      database-key = "postgresql"
    }
  } 
  database-postgresql-connection-pool = {
    "keycloak-pool" = {
      database-key = "postgresql"
      database-postgresql-user-key = "keycloak"
      database-database-key = "keycloak-db"
      database-postgresql-connection-pool-size = 13
      database-postgresql-connection-pool-name  = "keycloak-pool"
      database-postgresql-connection-pool-mode  = "session"
    }
  }
}
