terraform {
  source = "git@code.europa.eu:simpl/Operations/terraform-modules.git//kubectl?ref=kubectl-v0.0.1"
}

locals {
  # Load the relevant env.hcl file based on where terragrunt was invoked. This works because find_in_parent_folders
  # always works at the context of the child configuration.
  env_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  env_name = local.env_vars.locals.env

  instance_vars = read_terragrunt_config(find_in_parent_folders("instance.hcl"))
  instance_name = local.instance_vars.locals.instance

  region_vars = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  region_name = local.region_vars.locals.region
  region_global_name = local.region_vars.locals.region_global

  project_vars = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  project_id = local.project_vars.locals.project-id 

  global_prefix = "${local.env_name}-${local.region_name}-${local.instance_name}"
}

dependency "k8s" {
  config_path = "../k8s"

  # Configure mock outputs for the `validate` command that are returned when there are no outputs available (e.g the
  # module hasn't been applied yet.
  mock_outputs_allowed_terraform_commands = ["init","plan","validate"]
  mock_outputs = {
    k8s-host = "dummy-k8s-host"
    k8s-client-certificate = "ZHVtbXktdGVzdA=="
    k8s-client-ca-certificate = "ZHVtbXktdGVzdA=="
    k8s-client-key = "ZHVtbXktdGVzdA=="
  }
  mock_outputs_merge_strategy_with_state = "shallow"
}

dependencies {
  paths = ["../network","../gateway", "../k8s", "../helm"]
}

inputs = {
  # helm provider
  kubectl-host = dependency.k8s.outputs.k8s-host
  kubectl-client-certificate = base64decode(dependency.k8s.outputs.k8s-client-certificate)
  kubectl-client-key = base64decode(dependency.k8s.outputs.k8s-client-key)
  kubectl-cluster-ca-certificate = base64decode(dependency.k8s.outputs.k8s-client-ca-certificate)
}

