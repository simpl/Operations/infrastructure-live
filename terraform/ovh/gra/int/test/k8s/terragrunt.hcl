include "root" {
  path = find_in_parent_folders()
  expose = true
}

include "env" {
  path = "${get_terragrunt_dir()}/../../../../../_env/k8s.hcl"
}

inputs = {
  # K8S
  k8s-project-name = "testint-agents"
  k8s-project-default-vrack-gateway = ""
  k8s-project-private_network_routing_as_default = false
  k8s-project-version = "1.29"
  k8s-project-nodepool-flavor-name = "b3-32"
  k8s-project-nodepool-desired-nodes = 3
  k8s-project-nodepool-max-nodes = 5
  k8s-project-nodepool-min-nodes = 1
  k8s-project-nodepool-autoscale = true
}

